# MartosOlav_M17UF1R1
Replica del juego VVVVV hecho para el Reto #1 de la clase de **M17 Programació de Videojocs**

## Historia
Willy es un joven aventurero que fue encerrado en el interior de una extraña mazmorra por culpa de un cientifico tirano. El objetivo de Willy es salir de las profundidades de esta mazmorra.

## Descripción
- Plataforma: PC
- Genero: 2D, plataformas
- Idioma: Castellano
- Modo de juego: 1 Jugador
## Controles
Controlaras a Willy y le ayudaras a superar las adversidades.

- Moverse a la Izquierda: Tecla **[A]** o Tecla **[Flecha Izquierda]**
- Moverse a la Izquierda: Tecla **[D]** o Tecla **[Flecha Derecha]**
- Cambiar la gravedad: Tecla **[Espacio]**
- Pausar el juego: Tecla **[Escape]** o Tecla **[P]**

Mientras estes en el aire, ya sea yendo hacia arriba o cayendo, Willy podra correr en medio del aire, medio pausandose en el aire para posicionarse de mejor forma.

## Musica
- [Musica Sin Copyright de Aventura](https://www.youtube.com/watch?v=bDl0kNPkTiE&ab_channel=M%C3%BAsicasinCopyright)
- Sonidos de [Freesound.org](https://freesound.org/)
    - [Sonido de Muerte](https://freesound.org/people/MATRIXXX_/sounds/476469/)
    - [Sonido de Muerte por Pinchazo](https://freesound.org/people/GabrielaUPF/sounds/220290/)
    - [Sonido de Victoria](https://freesound.org/people/honeybone82/sounds/513253/)