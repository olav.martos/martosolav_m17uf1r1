using UnityEngine.SceneManagement;

public class MainMenu : Menu
{
    public void StartGame() { SceneManager.LoadScene(1); }
}
